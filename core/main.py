from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.responses import FileResponse
from datetime import datetime, date, timedelta
import jwt
import secrets

secret_key = secrets.token_urlsafe(32)

app = FastAPI()
security = HTTPBasic()

employees = {
    "alice": {
        "password": "123",
        "salary": 5000,
        "next_raise_date": date(2023, 12, 31)
    },
    "bob": {
        "password": "123",
        "salary": 6000,
        "next_raise_date": date(2022, 8, 16)
    },
}


TOKEN_EXPIRATION_MINUTES = 30


@app.get("/")
def root():
    return FileResponse("../templates/index.html")


@app.post("/token")
def login(credentials: HTTPBasicCredentials = Depends(security)):
    username = credentials.username
    password = credentials.password
    if username in employees and password == employees[username]["password"]:
        expiration = timedelta(minutes=TOKEN_EXPIRATION_MINUTES)
        token = generate_token(username, expiration)
        return {"token": token}
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid username or password",
        headers={"WWW-Authenticate": "Basic"},
    )


def generate_token(username: str, expiration: timedelta):
    payload = {"username": username, "exp": datetime.utcnow() + expiration}
    token = jwt.encode(payload, secret_key, algorithm="HS256")
    return token


@app.get("/salary")
def get_salary(token):
    username = validate_token(token)
    if username in employees:
        salary = employees[username]["salary"]
        next_raise_date = employees[username]["next_raise_date"]
        return {"salary": salary, "next_raise_date": next_raise_date}
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Employee not found",
    )


def validate_token(token: str):
    try:
        payload = jwt.decode(token, secret_key, algorithms=["HS256"])
        username = payload.get("username")
        exp = payload.get("exp")
        if not exp or datetime.utcnow() > datetime.fromtimestamp(exp):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Token expired",
                headers={"WWW-Authenticate": "Bearer"},
            )

        return username
    except jwt.exceptions.DecodeError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid token",
            headers={"WWW-Authenticate": "Bearer"},
        )
