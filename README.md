## Quick start
Install poetry
```bash
curl -sSL https://install.python-poetry.org | python3 -
```
Clone the project
```bash
git clone https://github.com/n1ce7ry/salary_checker
```

Go to the project folder and install the dependencies
```bash
cd salary_checker
poetry shell
poetry install
```
Run server
```bash
cd core
uvicorn main:app --reload
```
Go to 127.0.0.1:8000 and enter the test data: 
* User: alice
* Password: 123

## TO DO:
* Unit tests 
* Refactoring main.py
